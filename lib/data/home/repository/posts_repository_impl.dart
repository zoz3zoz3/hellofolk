import 'package:dartz/dartz.dart';
import 'package:hello_folk/data/core/repository/base_repository_impl.dart';
import 'package:hello_folk/data/home/datasource/posts_remote_datasource.dart';
import 'package:hello_folk/data/home/model/comments/comments_model.dart';
import 'package:hello_folk/data/home/model/post/post_model.dart';
import 'package:hello_folk/data/home/model/users/users_model/users_model.dart';
import 'package:hello_folk/domain/core/entities/failures.dart';
import 'package:hello_folk/domain/core/utils/constants.dart';
import 'package:hello_folk/domain/core/utils/network/network_info.dart';
import 'package:hello_folk/domain/home/entity/comment.dart';
import 'package:hello_folk/domain/home/entity/post.dart';
import 'package:hello_folk/domain/home/entity/user/user/user.dart';
import 'package:hello_folk/domain/home/repository/post_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:logger/logger.dart';

@LazySingleton(as: PostsRepository)
class PostsRepositoryImpl extends BaseRepositoryImpl
    implements PostsRepository {
  PostsRemoteDataSource remoteDataSource;
  NetworkInfo networkInfo;
  Logger logger;

  PostsRepositoryImpl(
    this.remoteDataSource,
    this.networkInfo,
    this.logger,
  ) : super(
          networkInfo,
          logger,
        );

  @override
  Future<Either<Failure, List<Post>>> getPosts() async {
    return await request(
      () async {
        try {
          final result = await remoteDataSource.getposts();

          return Right(result.map((e) => e.toDomain()).toList());
        } catch (e) {
          return Left(ServerFailure(errorCode: ServerErrorCode.serverError));
        }
      },
    );
  }

  @override
  Future<Either<Failure, List<Comment>>> getComments() async {
    return await request(
      () async {
        try {
          final result = await remoteDataSource.getcomments();

          return Right(result.map((e) => e.toDomain()).toList());
        } catch (e) {
          return Left(ServerFailure(errorCode: ServerErrorCode.serverError));
        }
      },
    );
  }

  @override
  Future<Either<Failure, List<User>>> getUsers() async {
    return await request(
      () async {
        try {
          final result = await remoteDataSource.getusers();

          return Right(result.map((e) => e.toDomain()).toList());
        } catch (e) {
          return Left(ServerFailure(errorCode: ServerErrorCode.serverError));
        }
      },
    );
  }
}
