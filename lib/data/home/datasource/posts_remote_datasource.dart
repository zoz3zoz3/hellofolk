import 'package:dio/dio.dart';
import 'package:hello_folk/data/core/utils/constants.dart';
import 'package:hello_folk/data/home/model/comments/comments_model.dart';
import 'package:hello_folk/data/home/model/post/post_model.dart';
import 'package:hello_folk/data/home/model/users/users_model/users_model.dart';
import 'package:injectable/injectable.dart';

import 'package:retrofit/retrofit.dart';

part 'posts_remote_datasource.g.dart';

abstract class PostsRemoteDataSource {
  Future<List<PostModel>> getposts();
  Future<List<CommentModel>> getcomments();
  Future<List<UsersModel>> getusers();
}

@LazySingleton(as: PostsRemoteDataSource)
@RestApi(baseUrl: '')
abstract class PostsRemoteDataSourceImpl implements PostsRemoteDataSource {
  @factoryMethod
  factory PostsRemoteDataSourceImpl(Dio dio) {
    return _PostsRemoteDataSourceImpl(dio, baseUrl: EndPoints.baseUrl);
  }

  @override
  @GET('/posts')
  Future<List<PostModel>> getposts();

  @override
  @GET('/comments')
  Future<List<CommentModel>> getcomments();

  @override
  @GET('/users')
  Future<List<UsersModel>> getusers();
}
