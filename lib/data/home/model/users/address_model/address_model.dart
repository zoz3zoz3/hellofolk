import 'package:hello_folk/data/home/model/users/geo_model/geo_model.dart';
import 'package:hello_folk/domain/home/entity/user/address/address.dart';
import 'package:json_annotation/json_annotation.dart';

part 'address_model.g.dart';

@JsonSerializable(createToJson: true)
class AddressModel {
  AddressModel({
    required this.street,
    required this.suite,
    required this.city,
    required this.zipcode,
    required this.geo,
  });

  final String street;
  final String suite;
  final String city;
  final String zipcode;
  final GeoModel geo;

  factory AddressModel.fromJson(Map<String, dynamic> json) =>
      _$AddressModelFromJson(json);

  Map<String, dynamic> toJson() => _$AddressModelToJson(this);
}

extension MapToDomain on AddressModel {
  Address toDomain() => Address(
        street: street,
        suite: suite,
        city: city,
        zipcode: zipcode,
        geo: geo.toDomain(),
      );
}
