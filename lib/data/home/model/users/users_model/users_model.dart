import 'package:hello_folk/data/home/model/users/address_model/address_model.dart';
import 'package:hello_folk/domain/home/entity/user/user/user.dart';
import 'package:json_annotation/json_annotation.dart';

part 'users_model.g.dart';

@JsonSerializable(createToJson: true)
class UsersModel {
  UsersModel({
    required this.id,
    required this.name,
    required this.username,
    required this.email,
    required this.address,
  });

  final int id;
  final String name;
  final String username;
  final String email;
  final AddressModel address;

  factory UsersModel.fromJson(Map<String, dynamic> json) =>
      _$UsersModelFromJson(json);

  Map<String, dynamic> toJson() => _$UsersModelToJson(this);
}

extension MapToDomain on UsersModel {
  User toDomain() => User(
        id: id,
        name: name,
        username: username,
        email: email,
        address: address.toDomain(),
      );
}
