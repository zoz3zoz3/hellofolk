import 'package:hello_folk/domain/home/entity/user/geo/geo.dart';
import 'package:json_annotation/json_annotation.dart';

part 'geo_model.g.dart';

@JsonSerializable(createToJson: true)
class GeoModel {
  GeoModel({
    required this.lat,
    required this.lng,
  });

  final String lat;
  final String lng;

  factory GeoModel.fromJson(Map<String, dynamic> json) =>
      _$GeoModelFromJson(json);

  Map<String, dynamic> toJson() => _$GeoModelToJson(this);
}

extension MapToDomain on GeoModel {
  Geo toDomain() => Geo(
        lat: lat,
        lng: lng,
      );
}
