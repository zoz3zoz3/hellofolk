import 'package:hello_folk/domain/home/entity/comment.dart';
import 'package:json_annotation/json_annotation.dart';

part 'comments_model.g.dart';

@JsonSerializable(createToJson: true)
class CommentModel {
  CommentModel({
    required this.postId,
    required this.id,
    required this.name,
    required this.email,
    required this.body,
  });

  final int postId;
  final int id;
  final String name;
  final String email;
  final String body;

  factory CommentModel.fromJson(Map<String, dynamic> json) =>
      _$CommentModelFromJson(json);

  Map<String, dynamic> toJson() => _$CommentModelToJson(this);
}

extension MapToDomain on CommentModel {
  Comment toDomain() => Comment(
        postId: postId,
        id: id,
        name: name,
        email: email,
        body: body,
      );
}
