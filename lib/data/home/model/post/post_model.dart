import 'package:hello_folk/domain/home/entity/post.dart';
import 'package:json_annotation/json_annotation.dart';

part 'post_model.g.dart';

@JsonSerializable(createToJson: true)
class PostModel {
  PostModel({
    required this.userId,
    required this.id,
    required this.title,
    required this.body,
  });

  final int userId;
  final int id;
  final String title;
  final String body;

  factory PostModel.fromJson(Map<String, dynamic> json) =>
      _$PostModelFromJson(json);

  Map<String, dynamic> toJson() => _$PostModelToJson(this);
}

extension MapToDomain on PostModel {
  Post toDomain() => Post(
        userId: userId,
        id: id,
        title: title,
        body: body,
      );
}
