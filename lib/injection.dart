// ignore_for_file: await_only_futures

import 'package:get_it/get_it.dart';
import 'package:hello_folk/injection.config.dart';
import 'package:injectable/injectable.dart';

final GetIt getIt = GetIt.instance;

@injectableInit
Future<void> configureInjection() async {
  await $initGetIt(getIt);
}

Future<void> resetInjection() async {
  await getIt.reset();
}
