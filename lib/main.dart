import 'package:flutter/material.dart';
import 'package:hello_folk/injection.dart';
import 'package:hello_folk/src/app.dart';

void main() async {
  await configureInjection();
  runApp(App());
}
