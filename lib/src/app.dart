import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hello_folk/presentation/core/routes/app_router.dart';
import 'package:hello_folk/presentation/core/utils/generated_assets/fonts.gen.dart';

class App extends StatelessWidget {
  App({Key? key}) : super(key: key);
  final _appRouter = AppRouter();
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(375, 812),
      builder: (context2, child) {
        return MaterialApp.router(
          routerDelegate: _appRouter.delegate(),
          routeInformationParser: _appRouter.defaultRouteParser(),
          title: 'Flutter Demo',
          theme: ThemeData(
              primarySwatch: Colors.purple,
              appBarTheme: const AppBarTheme(
                iconTheme: IconThemeData(color: Colors.black),
                elevation: 0,
              ),
              textTheme: Theme.of(context)
                  .textTheme
                  .apply(fontFamily: FontFamily.noah)),
        );
      },
    );
  }
}
