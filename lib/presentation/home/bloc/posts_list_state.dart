import 'package:hello_folk/domain/core/entities/failures.dart';
import 'package:hello_folk/domain/home/entity/comment.dart';
import 'package:hello_folk/domain/home/entity/post.dart';
import 'package:hello_folk/domain/home/entity/user/user/user.dart';

abstract class PostsListState {}

class PostsInitialState extends PostsListState {}

class PostsLoadingState extends PostsListState {}

class PostsErrorState extends PostsListState {
  Failure? failure;
  PostsErrorState({this.failure});
}

class PostsSuccessState extends PostsListState {
  List<Post> posts;
  List<User> users;
  List<Comment> comments;
  PostsSuccessState(this.posts, this.comments, this.users);
}
