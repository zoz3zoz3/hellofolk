import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hello_folk/domain/core/usecase/usecase.dart';
import 'package:hello_folk/domain/home/usecase/get_comments_usecase.dart';
import 'package:hello_folk/domain/home/usecase/get_posts_usecase.dart';
import 'package:hello_folk/domain/home/usecase/get_users_usecase.dart';
import 'package:hello_folk/presentation/home/bloc/posts_list_event.dart';
import 'package:hello_folk/presentation/home/bloc/posts_list_state.dart';
import 'package:injectable/injectable.dart';

@injectable
class PostsListBloc extends Bloc<PostsEvent, PostsListState> {
  GetPostsListUseCase getPostsListUseCase;
  GetUsersUseCase getUsersUseCase;
  GetCommentsUseCase getCommentsUseCase;

  PostsListBloc(
    this.getPostsListUseCase,
    this.getCommentsUseCase,
    this.getUsersUseCase,
  ) : super(PostsInitialState()) {
    on<GetDataEvent>(
      (event, emit) async {
        emit(PostsLoadingState());

        final postsResponse = await getPostsListUseCase(NoParams());
        final commentsResponse = await getCommentsUseCase(NoParams());
        final usersResponse = await getUsersUseCase(NoParams());

        postsResponse.fold(
          (l) {
            emit(PostsErrorState(failure: l));
          },
          (posts) {
            commentsResponse.fold(
              (l) {
                emit(PostsErrorState(failure: l));
              },
              (comments) {
                usersResponse.fold(
                  (l) {
                    emit(PostsErrorState(failure: l));
                  },
                  (users) {
                    emit(PostsSuccessState(posts, comments, users));
                  },
                );
              },
            );
          },
        );
      },
    );
  }
}
