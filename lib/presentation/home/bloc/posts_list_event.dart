abstract class PostsEvent {}

class GetDataEvent extends PostsEvent {}

class HandleDataEvent extends PostsEvent {}
