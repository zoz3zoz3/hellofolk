import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hello_folk/domain/home/entity/comment.dart';
import 'package:hello_folk/domain/home/entity/post.dart';
import 'package:hello_folk/domain/home/entity/user/user/user.dart';
import 'package:hello_folk/presentation/core/routes/app_router.dart';
import 'package:hello_folk/presentation/core/utils/generated_assets/assets.gen.dart';

class PostListItem extends StatelessWidget {
  final Post post;
  final User user;
  final List<Comment> comments;

  const PostListItem({
    Key? key,
    required this.post,
    required this.user,
    required this.comments,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        AutoRouter.of(context).push(
          PostDetailsPageRoute(
            comments: comments,
            post: post,
            user: user,
          ),
        );
      },
      child: Container(
        color: Colors.white,
        padding: const EdgeInsetsDirectional.fromSTEB(30, 15, 17, 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                SvgPicture.asset(
                  Assets.icons.postIcon.path,
                ),
                const SizedBox(width: 14),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(user.name),
                    Text(user.email),
                  ],
                ),
              ],
            ),
            Text(
              post.body.replaceAll("\n", ""),
              maxLines: 2,
              textAlign: TextAlign.start,
              softWrap: true,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );
  }
}
