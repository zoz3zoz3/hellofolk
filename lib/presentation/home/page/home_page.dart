// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hello_folk/injection.dart';
import 'package:hello_folk/presentation/core/utils/constants.dart';
import 'package:hello_folk/presentation/core/utils/generated_assets/assets.gen.dart';
import 'package:hello_folk/presentation/core/utils/screen_utils.dart';
import 'package:hello_folk/presentation/core/widgets/app_name_logo_widget.dart';
import 'package:hello_folk/presentation/home/bloc/posts_list_bloc.dart';
import 'package:hello_folk/presentation/home/bloc/posts_list_event.dart';
import 'package:hello_folk/presentation/home/bloc/posts_list_state.dart';
import 'package:hello_folk/presentation/home/widgets/post_list_item.dart';
import 'package:lottie/lottie.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with ScreenUtils {
  late PostsListBloc postsBloc;

  @override
  void initState() {
    postsBloc = getIt<PostsListBloc>();
    postsBloc.add(GetDataEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: Column(
        children: [
          Container(
            height: 150.h,
            width: double.infinity,
            decoration: BoxDecoration(
              color: AppColors.purpul,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(20),
                bottomRight: Radius.circular(20),
              ),
            ),
            padding: EdgeInsets.fromLTRB(22, 40, 22, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(Assets.icons.burgerIcon.path),
                const AppLogoNameWidget(
                  isLastsplash: true,
                ),
                Icon(
                  Icons.search,
                  size: 20.w,
                  color: Colors.white,
                ),
              ],
            ),
          ),
          BlocListener<PostsListBloc, PostsListState>(
            bloc: postsBloc,
            listener: (context, state) {},
            child: Expanded(
              child: BlocBuilder(
                bloc: postsBloc,
                builder: (context, PostsListState state) {
                  if (state is PostsLoadingState) {
                    return Lottie.asset(Assets.animations.twoDots);
                  } else if (state is PostsErrorState) {
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(getErrorMssage(state.failure)),
                          ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(AppColors.purpul),
                            ),
                            onPressed: () {
                              postsBloc.add(GetDataEvent());
                            },
                            child: const Text('Try Again'),
                          )
                        ],
                      ),
                    );
                  } else if (state is PostsSuccessState) {
                    return ListView.separated(
                      padding: EdgeInsets.zero,
                      itemCount: state.posts.length,
                      itemBuilder: (ctx, index) {
                        return PostListItem(
                          post: state.posts[index],
                          user: state.users.firstWhere(
                            (user) => state.posts[index].userId == user.id,
                          ),
                          comments: state.comments
                              .where((e) => e.postId == state.posts[index].id)
                              .toList(),
                        );
                      },
                      separatorBuilder: (ctx, index) {
                        return const SizedBox(height: 14);
                      },
                    );
                  } else {
                    return const SizedBox.shrink();
                  }
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
