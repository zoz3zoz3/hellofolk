// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hello_folk/presentation/core/utils/generated_assets/assets.gen.dart';
import 'package:lottie/lottie.dart';

class OnBoardingPage extends StatefulWidget {
  final void Function()? move;
  const OnBoardingPage({
    Key? key,
    this.move,
  }) : super(key: key);

  @override
  State<OnBoardingPage> createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends State<OnBoardingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          children: [
            Column(
              children: [
                Expanded(
                  flex: 2,
                  child: Lottie.asset(
                    Assets.animations.characterDrawInTheAir,
                    repeat: true,
                  ),
                ),
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 28),
                  child: Column(
                    children: const [
                      Text(
                        'share your news \nwith world ',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Velit vulputate ornare magna integer. Ullamcorper quam fermentum, in pellentesque. Velit magna luctus semper curabitur malesuada eget. Vel sit neque, tristique sit.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.normal,
                        ),
                      )
                    ],
                  ),
                ))
              ],
            ),
            GestureDetector(
              onTap: widget.move,
              child: Align(
                  alignment: Alignment.bottomRight,
                  child: SizedBox(
                    width: 108,
                    height: 80,
                    child: Stack(
                      alignment: Alignment.bottomRight,
                      children: [
                        SvgPicture.asset(
                          Assets.icons.yelloBubble.path,
                          fit: BoxFit.cover,
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
