import 'package:flutter/material.dart';
import 'package:hello_folk/presentation/on_boarding/page/on_boarding_page.dart';
import 'package:hello_folk/presentation/on_boarding/page/on_boarding_second_page.dart';

class OnBoardingPageWrapper extends StatefulWidget {
  const OnBoardingPageWrapper({Key? key}) : super(key: key);

  @override
  State<OnBoardingPageWrapper> createState() => _OnBoardingPageWrapperState();
}

class _OnBoardingPageWrapperState extends State<OnBoardingPageWrapper> {
  PageController pageController = PageController();
  @override
  void initState() {
    WidgetsFlutterBinding.ensureInitialized();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: pageController,
        children: [
          OnBoardingPage(
            move: () {
              pageController.animateToPage(
                1,
                duration: const Duration(milliseconds: 500),
                curve: Curves.linear,
              );
            },
          ),
          OnBoardingSecondPage(
            move: () {
              pageController.animateToPage(
                0,
                duration: const Duration(milliseconds: 500),
                curve: Curves.linear,
              );
            },
          ),
        ],
      ),
    );
  }
}
