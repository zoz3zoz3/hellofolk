import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hello_folk/presentation/core/routes/app_router.dart';
import 'package:hello_folk/presentation/core/utils/constants.dart';
import 'package:hello_folk/presentation/core/utils/generated_assets/assets.gen.dart';
import 'package:hello_folk/presentation/core/widgets/app_name_logo_widget.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  Timer? timer;
  int index = 0;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(
        const Duration(milliseconds: 700), (Timer t) => changeColor());
  }

  changeColor() {
    setState(
      () {
        if (index != AppColors.splashColors.length - 1) {
          index += 1;
        } else {
          Future.delayed(const Duration(milliseconds: 700)).then((value) {
            timer?.cancel();
            AutoRouter.of(context).replace(const OnBoardingPageWrapperRoute());
          });
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedContainer(
        duration: const Duration(milliseconds: 100),
        color: AppColors.splashColors[index],
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(
                index != AppColors.splashColors.length - 1
                    ? Assets.icons.whiteLogo.path
                    : Assets.icons.coloredLogo.path,
              ),
              AppLogoNameWidget(
                isLastsplash: index != AppColors.splashColors.length - 1,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
