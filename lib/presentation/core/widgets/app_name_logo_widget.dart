import 'package:flutter/material.dart';
import 'package:hello_folk/presentation/core/utils/constants.dart';

class AppLogoNameWidget extends StatelessWidget {
  final bool? isLastsplash;
  const AppLogoNameWidget({Key? key, this.isLastsplash}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          "HelloFolk",
          style: TextStyle(
            fontSize: 30,
            height: 1,
            fontWeight: FontWeight.bold,
            color: isLastsplash ?? false ? AppColors.white : Colors.black,
          ),
        ),
        SizedBox(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Tessafold",
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontSize: 12,
                  height: 1,
                  color: isLastsplash ?? false ? AppColors.white : Colors.black,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
