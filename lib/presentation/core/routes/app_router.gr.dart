// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

part of 'app_router.dart';

class _$AppRouter extends RootStackRouter {
  _$AppRouter([GlobalKey<NavigatorState>? navigatorKey]) : super(navigatorKey);

  @override
  final Map<String, PageFactory> pagesMap = {
    SplashPageRoute.name: (routeData) {
      return MaterialPageX<dynamic>(
          routeData: routeData, child: const SplashPage());
    },
    OnBoardingPageWrapperRoute.name: (routeData) {
      return MaterialPageX<dynamic>(
          routeData: routeData, child: const OnBoardingPageWrapper());
    },
    HomePageRoute.name: (routeData) {
      return MaterialPageX<dynamic>(
          routeData: routeData, child: const HomePage());
    },
    PostDetailsPageRoute.name: (routeData) {
      final args = routeData.argsAs<PostDetailsPageRouteArgs>();
      return MaterialPageX<dynamic>(
          routeData: routeData,
          child: PostDetailsPage(
              key: args.key,
              comments: args.comments,
              post: args.post,
              user: args.user));
    }
  };

  @override
  List<RouteConfig> get routes => [
        RouteConfig(SplashPageRoute.name, path: '/'),
        RouteConfig(OnBoardingPageWrapperRoute.name,
            path: '/on-boarding-page-wrapper'),
        RouteConfig(HomePageRoute.name, path: '/home-page'),
        RouteConfig(PostDetailsPageRoute.name, path: '/post-details-page')
      ];
}

/// generated route for
/// [SplashPage]
class SplashPageRoute extends PageRouteInfo<void> {
  const SplashPageRoute() : super(SplashPageRoute.name, path: '/');

  static const String name = 'SplashPageRoute';
}

/// generated route for
/// [OnBoardingPageWrapper]
class OnBoardingPageWrapperRoute extends PageRouteInfo<void> {
  const OnBoardingPageWrapperRoute()
      : super(OnBoardingPageWrapperRoute.name,
            path: '/on-boarding-page-wrapper');

  static const String name = 'OnBoardingPageWrapperRoute';
}

/// generated route for
/// [HomePage]
class HomePageRoute extends PageRouteInfo<void> {
  const HomePageRoute() : super(HomePageRoute.name, path: '/home-page');

  static const String name = 'HomePageRoute';
}

/// generated route for
/// [PostDetailsPage]
class PostDetailsPageRoute extends PageRouteInfo<PostDetailsPageRouteArgs> {
  PostDetailsPageRoute(
      {Key? key,
      required List<Comment> comments,
      required Post post,
      required User user})
      : super(PostDetailsPageRoute.name,
            path: '/post-details-page',
            args: PostDetailsPageRouteArgs(
                key: key, comments: comments, post: post, user: user));

  static const String name = 'PostDetailsPageRoute';
}

class PostDetailsPageRouteArgs {
  const PostDetailsPageRouteArgs(
      {this.key,
      required this.comments,
      required this.post,
      required this.user});

  final Key? key;

  final List<Comment> comments;

  final Post post;

  final User user;

  @override
  String toString() {
    return 'PostDetailsPageRouteArgs{key: $key, comments: $comments, post: $post, user: $user}';
  }
}
