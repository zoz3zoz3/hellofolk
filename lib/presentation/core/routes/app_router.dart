import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:hello_folk/domain/home/entity/comment.dart';
import 'package:hello_folk/domain/home/entity/post.dart';
import 'package:hello_folk/domain/home/entity/user/user/user.dart';
import 'package:hello_folk/presentation/home/page/home_page.dart';
import 'package:hello_folk/presentation/on_boarding/page/on_boarding_page_wrapper.dart';
import 'package:hello_folk/presentation/post_detail/post_detail_page.dart';
import 'package:hello_folk/presentation/splash/splash_page.dart';

part 'app_router.gr.dart';

@MaterialAutoRouter(
  routes: <AutoRoute>[
    AutoRoute(page: SplashPage, initial: true),
    AutoRoute(page: OnBoardingPageWrapper),
    AutoRoute(page: HomePage),
    AutoRoute(page: PostDetailsPage),
  ],
)
class AppRouter extends _$AppRouter {}
