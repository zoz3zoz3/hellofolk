import 'package:flutter/material.dart';
import 'package:hello_folk/domain/core/entities/failures.dart';
import 'package:hello_folk/domain/core/utils/constants.dart';

mixin ScreenUtils<T extends StatefulWidget> on State<T> {
  ScaffoldFeatureController<SnackBar, SnackBarClosedReason>? handleError(
      {Failure? failure,
      String? customMessage,
      Map<ServerErrorCode, String>? customMessages,
      bool isFloating = false}) {
    return showError(
        failure: failure,
        customMessage: customMessage,
        customMessages: customMessages,
        isFloating: isFloating);
  }

  String getErrorMssage(Failure? failure) {
    String message = 'somthing went wrong';

    if (failure != null && failure is ServerFailure) {
      if (failure.errorCode == ServerErrorCode.noInternetConnection) {
        message = 'No Internet connnection';
      } else if (failure.errorCode == ServerErrorCode.forbidden) {
        message = 'access denied';
      } else if (failure.errorCode == ServerErrorCode.unauthenticated) {
        message = 'unauthenticated';
      } else if (failure.message.isNotEmpty) {
        message = failure.message;
      } else {
        message = 'somthing went wrong';
      }
    }
    return message;
  }

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> showError(
      {Failure? failure,
      String? customMessage,
      Map<ServerErrorCode, String>? customMessages,
      bool isFloating = false}) {
    String message = customMessage ?? 'somthing went wrong';

    if (failure != null && failure is ServerFailure) {
      if (failure.errorCode == ServerErrorCode.noInternetConnection) {
        message = 'No Internet connnection';
      } else if (failure.errorCode == ServerErrorCode.forbidden) {
        message = 'access denied';
      } else if (failure.errorCode == ServerErrorCode.unauthenticated) {
        message = 'unauthenticated';
      } else if (failure.message.isNotEmpty) {
        message = failure.message;
      } else if (customMessages != null && customMessages.isNotEmpty) {
        message = customMessages[failure.errorCode] ?? 'somthing went wrong';
      }
    }

    ScaffoldMessenger.of(context).hideCurrentSnackBar();
    return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: Theme.of(context).errorColor,
      behavior: isFloating ? SnackBarBehavior.floating : null,
    ));
  }

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> showSuccess(
      {String? customMessage, bool isFloating = false}) {
    String message = customMessage ?? 'success';

    ScaffoldMessenger.of(context).hideCurrentSnackBar();
    return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: Theme.of(context).colorScheme.secondary,
      behavior: isFloating ? SnackBarBehavior.floating : null,
    ));
  }
}
