import 'package:flutter/material.dart';

class AppColors {
  static Color yellow = const Color(0xffFBAD18);
  static Color purpul = const Color(0xff5F4090);
  static Color lightGreen = const Color(0xff02D5B4);
  static Color white = const Color(0xffffffff);

  static List<Color> splashColors = [yellow, purpul, lightGreen, white];
}
