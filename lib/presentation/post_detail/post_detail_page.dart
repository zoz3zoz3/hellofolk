import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hello_folk/domain/home/entity/comment.dart';
import 'package:hello_folk/domain/home/entity/post.dart';
import 'package:hello_folk/domain/home/entity/user/user/user.dart';
import 'package:hello_folk/presentation/core/utils/constants.dart';

import '../core/utils/generated_assets/assets.gen.dart';

class PostDetailsPage extends StatefulWidget {
  final Post post;
  final User user;
  final List<Comment> comments;
  const PostDetailsPage({
    Key? key,
    required this.comments,
    required this.post,
    required this.user,
  }) : super(key: key);

  @override
  State<PostDetailsPage> createState() => _PostDetailsPageState();
}

class _PostDetailsPageState extends State<PostDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffF2F2F2),
      appBar: AppBar(
        backgroundColor: AppColors.white,
        actions: const [
          Icon(
            Icons.bookmark_outline,
            color: Colors.black,
          ),
        ],
      ),
      body: Column(
        children: [
          const SizedBox(height: 14),
          Container(
            color: Colors.white,
            padding: const EdgeInsetsDirectional.fromSTEB(30, 15, 17, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    SvgPicture.asset(
                      Assets.icons.postIcon.path,
                    ),
                    const SizedBox(width: 14),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(widget.user.name),
                        Text(widget.user.email),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  // height: 311.h,
                  width: 320.w,
                  child: Text(
                    widget.post.body.replaceAll("\n", "") * 4,
                    textAlign: TextAlign.start,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 14),
          Expanded(
            child: Container(
              color: Colors.white,
              padding: const EdgeInsets.symmetric(vertical: 16),
              child: ListView.separated(
                separatorBuilder: (ctx, index) => const SizedBox(height: 10),
                itemBuilder: (ctx, index) => Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SvgPicture.asset(
                      Assets.icons.postIcon.path,
                    ),
                    const SizedBox(width: 4),
                    Container(
                      padding: const EdgeInsets.all(6),
                      decoration: const BoxDecoration(
                        color: Color(0xffF4F4F4),
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(widget.user.name),
                          Text(widget.comments[index].email),
                          const SizedBox(height: 7),
                          SizedBox(
                            width: 282.w,
                            child: Text(
                              widget.comments[index].body,
                              softWrap: true,
                              textAlign: TextAlign.justify,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                itemCount: widget.comments.length,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
