import 'package:hello_folk/domain/home/entity/user/address/address.dart';

class User {
  User({
    required this.id,
    required this.name,
    required this.username,
    required this.email,
    required this.address,
  });

  final int id;
  final String name;
  final String username;
  final String email;
  final Address address;
}
