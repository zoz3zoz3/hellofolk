class Geo {
  Geo({
    required this.lat,
    required this.lng,
  });

  final String lat;
  final String lng;
}
