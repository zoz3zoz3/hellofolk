import 'package:dartz/dartz.dart';
import 'package:hello_folk/domain/core/entities/failures.dart';
import 'package:hello_folk/domain/core/repository/base_repository.dart';
import 'package:hello_folk/domain/home/entity/comment.dart';
import 'package:hello_folk/domain/home/entity/post.dart';
import 'package:hello_folk/domain/home/entity/user/user/user.dart';

abstract class PostsRepository implements BaseRepository {
  Future<Either<Failure, List<Post>>> getPosts();
  Future<Either<Failure, List<Comment>>> getComments();
  Future<Either<Failure, List<User>>> getUsers();
}
