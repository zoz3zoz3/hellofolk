import 'package:dartz/dartz.dart';
import 'package:hello_folk/domain/core/entities/failures.dart';
import 'package:hello_folk/domain/core/usecase/usecase.dart';
import 'package:hello_folk/domain/home/entity/post.dart';
import 'package:hello_folk/domain/home/repository/post_repository.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class GetPostsListUseCase extends UseCase<List<Post>, NoParams> {
  final PostsRepository repository;

  GetPostsListUseCase(this.repository);
  @override
  Future<Either<Failure, List<Post>>> call(NoParams params) {
    return repository.getPosts();
  }
}
