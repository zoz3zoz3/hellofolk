import 'package:hello_folk/domain/core/entities/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:hello_folk/domain/core/usecase/usecase.dart';
import 'package:hello_folk/domain/home/entity/comment.dart';
import 'package:hello_folk/domain/home/repository/post_repository.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class GetCommentsUseCase extends UseCase<List<Comment>, NoParams> {
  PostsRepository repository;

  GetCommentsUseCase(this.repository);

  @override
  Future<Either<Failure, List<Comment>>> call(NoParams params) {
    return repository.getComments();
  }
}
