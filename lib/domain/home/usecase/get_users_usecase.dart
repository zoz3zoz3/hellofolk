import 'package:hello_folk/domain/core/entities/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:hello_folk/domain/core/usecase/usecase.dart';
import 'package:hello_folk/domain/home/entity/user/user/user.dart';
import 'package:hello_folk/domain/home/repository/post_repository.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class GetUsersUseCase extends UseCase<List<User>, NoParams> {
  PostsRepository repository;

  GetUsersUseCase(this.repository);

  @override
  Future<Either<Failure, List<User>>> call(NoParams params) {
    return repository.getUsers();
  }
}
