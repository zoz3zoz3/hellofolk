import 'package:dartz/dartz.dart';
import 'package:hello_folk/domain/core/entities/failures.dart';

typedef FutureEitherFailureOrData<T> = Future<Either<Failure, T>> Function();

abstract class BaseRepository {
  Future<Either<Failure, T>> request<T>(
    FutureEitherFailureOrData<T> body,
  );
}
