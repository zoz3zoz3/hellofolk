// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i3;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:internet_connection_checker/internet_connection_checker.dart'
    as _i4;
import 'package:logger/logger.dart' as _i5;

import 'data/core/utils/network/network_info.dart' as _i7;
import 'data/home/datasource/posts_remote_datasource.dart' as _i8;
import 'data/home/repository/posts_repository_impl.dart' as _i10;
import 'domain/core/utils/network/network_info.dart' as _i6;
import 'domain/home/repository/post_repository.dart' as _i9;
import 'domain/home/usecase/get_comments_usecase.dart' as _i11;
import 'domain/home/usecase/get_posts_usecase.dart' as _i12;
import 'domain/home/usecase/get_users_usecase.dart' as _i13;
import 'injectable_module.dart' as _i15;
import 'presentation/home/bloc/posts_list_bloc.dart'
    as _i14; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final injectableModule = _$InjectableModule();
  gh.lazySingleton<_i3.Dio>(() => injectableModule.dioInstance);
  gh.lazySingleton<_i4.InternetConnectionChecker>(
      () => injectableModule.connectionChecker);
  gh.lazySingleton<_i5.Logger>(() => injectableModule.logger);
  gh.lazySingleton<_i6.NetworkInfo>(
      () => _i7.NetworkInfoImpl(get<_i4.InternetConnectionChecker>()));
  gh.lazySingleton<_i8.PostsRemoteDataSource>(
      () => _i8.PostsRemoteDataSourceImpl(get<_i3.Dio>()));
  gh.lazySingleton<_i9.PostsRepository>(() => _i10.PostsRepositoryImpl(
      get<_i8.PostsRemoteDataSource>(),
      get<_i6.NetworkInfo>(),
      get<_i5.Logger>()));
  gh.lazySingleton<_i11.GetCommentsUseCase>(
      () => _i11.GetCommentsUseCase(get<_i9.PostsRepository>()));
  gh.lazySingleton<_i12.GetPostsListUseCase>(
      () => _i12.GetPostsListUseCase(get<_i9.PostsRepository>()));
  gh.lazySingleton<_i13.GetUsersUseCase>(
      () => _i13.GetUsersUseCase(get<_i9.PostsRepository>()));
  gh.factory<_i14.PostsListBloc>(() => _i14.PostsListBloc(
      get<_i12.GetPostsListUseCase>(),
      get<_i11.GetCommentsUseCase>(),
      get<_i13.GetUsersUseCase>()));
  return get;
}

class _$InjectableModule extends _i15.InjectableModule {}
